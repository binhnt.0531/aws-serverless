import {assert} from 'chai';
import {suite, test, timeout} from "mocha-typescript";
import {createOrUpdatePost, getPosts} from "../src/handler";
import * as AWS from '../node_modules/aws-sdk';


const input = {title: 'demo title', body: 'demo body'};

@suite
class ServerlessTest {
  callBack = () => {
  };

  before() {
    AWS.config.update({
      region: process.env['aws_region'] || 'ap-southeast-1',
      credentials: new AWS.Credentials({
        accessKeyId: process.env['aws_access_key_id'] || '',
        secretAccessKey: process.env['aws_secret_access_key'] || ''
      })
    });
  }

  @test('get Post List', timeout(300000))
  test001() {
    return this.testPostAPI(getPosts(null, null, this.callBack));
  }

  @test('create/update Post', timeout(300000))
  test002() {
    return this.testPostAPI(createOrUpdatePost(input, null, this.callBack));
  }

  private async testPostAPI(process: Promise<any>) {
    try {
      await process;
      assert.isTrue(true);
    } catch {
      assert.isTrue(false, 'An error occurred')
    }
  }
}
