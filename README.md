# aws-serverless-api

## Requirements

* AWS CLI already configured with Administrator permission
* [SAM installed](https://aws.amazon.com/vi/serverless/sam/)
* [NodeJS 8.10+ installed](https://nodejs.org/en/download/)

## Setup process

~~~
npm install
~~~

## Test
~~~
aws_access_key_id=<value> aws_secret_access_key=<value> npm run test
~~~
- awsAccessKeyId: access key id of IAM user 
- awsSecretAccessKey: secret access key for an IAM user
- Test report HTML saved to `test-report/index.html`
- Coverage report HTML saved to `coverage/index.html`

## Building the project
Run the following command to build and deploy to aws

~~~
env=<value> bucket=<value> [iam_role=<value>] ./deploy.sh
~~~
- env: dev, stg, prod
- bucket: The Amazon s3 bucket (eg: cf-deploy-serverless)
- iam_role: The Amazon Resource Name (ARN) of an AWS Identity and Access Management (IAM) role
