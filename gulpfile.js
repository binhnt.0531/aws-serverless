let gulp = require('gulp');
let del = require('del');
let shell = require('gulp-shell');
let argv = require('yargs').argv;
let install = require('gulp-install');
let runSequence = require('run-sequence');
runSequence.options.ignoreUndefinedTasks = true;

const PATH = {
    'build': './build',
    'dist': './build/dist'
};

function clean(callback) {
    return del(PATH.build, callback);
}

function npm_init() {
    return gulp.src(['./package.json'])
        .pipe(gulp.dest(PATH.dist))
        .pipe(install({production: true}));
}

//--------------------------------------//

gulp.task('clean', function (callback) {
    return clean(callback)
});

gulp.task('tsc', shell.task('tsc'));

gulp.task('npm', function () {
    return npm_init()
});

gulp.task('build', function (callback) {
    return runSequence(['clean'], ['tsc', 'npm'], callback);
});

//--------------------------------------//
