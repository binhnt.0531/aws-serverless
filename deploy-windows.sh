#!/usr/bin/env bash
set -e

if [ -z "${env}" ]; then
    echo "env is required and allowed values 'dev, stg, prod'"
    exit 2
fi

if [ -z "${bucket}" ]; then
    echo "bucket is required"
    exit 2
fi

# load environment values
envFileName="./environment/${env}.env"
parameter="env=${env}"
while IFS= read line
do
	parameter="${parameter} ${line}"
done <"$envFileName"

# build project
npm run build

# package and deploy
sam="winpty /C/Program\ Files/Amazon/AWSSAMCLI/bin/sam.cmd"
packageCmd="${sam} package --template-file template.yaml --output-template-file cfn-template.yaml --s3-bucket ${bucket}"
deployCmd="${sam} deploy --template-file cfn-template.yaml --stack-name ${env}-post-api-stack --capabilities CAPABILITY_NAMED_IAM --parameter-overrides ${parameter}"

if [ ! -z "${iam_role}" ]; then
    deployCmd="${deployCmd} --role-arn ${iam_role}"
fi

eval ${packageCmd}
eval ${deployCmd}
