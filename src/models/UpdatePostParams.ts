export class UpdatePostParams {
  id?: string;
  title: string;
  body: string;
}
