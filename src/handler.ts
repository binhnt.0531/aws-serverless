import {PostTableDynamoDB} from "./services/PostTableDynamoDB";
import {UpdatePostParams} from "./models/UpdatePostParams";

export async function getPosts(event, context, callback) {
  try {
    const dynamoDB = new PostTableDynamoDB();
    const result = await dynamoDB.scanAllData();
    callback(null, {
      statusCode: 200,
      body: JSON.stringify(result)
    });
  } catch (e) {
    callback(new ProcessException(JSON.stringify({message: e.message, stack: e.stack})))
  }
}

export async function createOrUpdatePost(event, context, callback) {
  try {
    const inputBody: UpdatePostParams = event && event.body ? JSON.parse(event.body) : event;
    if (!inputBody || !inputBody.title || !inputBody.body) {
      callback(null, {
        statusCode: 400,
        body: JSON.stringify({message: "'title' and 'body' values must not be null!"})
      });
      return;
    }
    const dynamoDB = new PostTableDynamoDB();
    let response;
    if (inputBody.id) {
      response = await dynamoDB.updateData(PostTableDynamoDB.createUpdatePostParams(inputBody));
    } else {
      const params = PostTableDynamoDB.createNewPostParams(inputBody);
      await dynamoDB.insertData(params);
      response = params.Item;
    }
    callback(null, {
      statusCode: 200,
      body: JSON.stringify(response)
    });
  } catch (e) {
    callback(new ProcessException(JSON.stringify({message: e.message, stack: e.stack})))
  }
}

class ProcessException extends Error {
}


