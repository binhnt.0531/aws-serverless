import * as AWS from "aws-sdk";
import {DocumentClient} from "aws-sdk/lib/dynamodb/document_client";
import {ENV} from "../models/Env";

const uuidv4 = require('uuid/v4');
const POST_TABLE_NAME = `${ENV}-Posts`;

export class PostTableDynamoDB {
  private documentClient: DocumentClient;


  constructor() {
    this.documentClient = new AWS.DynamoDB.DocumentClient();
  }

  scanAllData() {
    return this.documentClient.scan({
      TableName: POST_TABLE_NAME
    }).promise();
  }

  insertData(param) {
    return this.documentClient.put(param).promise();
  }

  updateData(param) {
    return this.documentClient.update(param).promise();
  }

  static createNewPostParams(input) {
    const currentTime = new Date().getTime();
    return {
      TableName: POST_TABLE_NAME,
      Item: {
        Id: uuidv4(),
        Title: input.title,
        Body: input.body,
        CreatedAt: currentTime,
        UpdatedAt: currentTime
      }
    }
  }

  static createUpdatePostParams(input) {
    return {
      TableName: POST_TABLE_NAME,
      Key: {
        Id: input.id
      },
      UpdateExpression: "set Title = :title, Body = :body, UpdatedAt = :updatedAt",
      ExpressionAttributeValues: {
        ':title': input.title,
        ':body': input.body,
        ':updatedAt': new Date().getTime()
      },
      ReturnValues: "UPDATED_NEW"
    };
  }
}
